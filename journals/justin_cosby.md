# Journal

# Week 1:

- Excalidraw (Brainstorm and designed our project)
- Confirmed 3rd API party links(ESPN and SeatGeek)
- Created Database
- Configured Tables on Migrations
- Authentication for the backend created
- Created User Routes, Models, and Queries
- Created Team Routes, Models, and Queries(ESPN API)
- Created Game Routes, Models, and Queries(ESPN API)

# Week 2:

- Learn to scrape due to API shortcomings
- Get tickets from API/scrapings of TickPick
- Added 3rd party ticket APIs to back end
- Error Handling
- Created Ticket Routes, Models, and Queries

# Week 3:

- Gut Auth Provided Code
- Setup Redux Auth
- Create all front-end components besides homepage and game details

# Week 4:

- Homepage and game details component
- Delete user added
- Unit Testing
- Readme
- CSS
- Clean Code
